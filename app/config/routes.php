<?php

return [
    '' => [
        'controller' => 'page',
        'action' => 'index'
    ],

    'user/registration' => [
        'controller' => 'user',
        'action' => 'registration'
    ],

    'user/login' => [
        'controller' => 'user',
        'action' => 'login'
    ],

    'user/home' => [
        'controller' => 'user',
        'action' => 'home'
    ],

    'user/search' => [
        'controller' => 'user',
        'action' => 'search'
    ],

    'user/inform' => [
        'controller' => 'user',
        'action' => 'inform'
    ],

    'user/profile' => [
        'controller' => 'user',
        'action' => 'profile'
    ]
];