<?php 

namespace App\controllers;


use App\core\Controller;
use Twig\Error\LoaderError;


class PageController extends Controller
{
    public function indexAction()
    {
        try
        {
            echo $this->render()->render('index.twig');
        } catch (LoaderError $e)
        {
            echo $e;
        }
    }

}