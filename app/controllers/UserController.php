<?php


namespace App\Controllers;

use App\core\Controller;
use Twig\Error\LoaderError;


class UserController extends Controller
{

    public function loginAction()
    {
        try
        {
            echo $this->render()->render('login.twig');
        } catch (LoaderError $e)
        {
            echo $e;
        }
    }

    public function registrationAction()
    {
        try
        {
            echo $this->render()->render('registration.twig');
        } catch (LoaderError $e)
        {
            var_dump($e);
        }
    }
    public function homeAction()
    {
        try
        {
            echo $this->render()->render('home.twig');
        } catch (LoaderError $e)
        {
            var_dump($e);
        }
    }
    public function searchAction()
    {
        try
        {
            echo $this->render()->render('search.twig');
        } catch (LoaderError $e)
        {
            var_dump($e);
        }
    }
    public function informAction()
    {
        try
        {
            echo $this->render()->render('inform.twig');
        } catch (LoaderError $e)
        {
            var_dump($e);
        }
    }
    public function profileAction()
    {
        try
        {
            echo $this->render()->render('profile.twig');
        } catch (LoaderError $e)
        {
            var_dump($e);
        }
    }

}