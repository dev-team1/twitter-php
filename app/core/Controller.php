<?php

namespace App\core;

require_once '../vendor/autoload.php';

use App\core\View;
use Twig\Loader\FilesystemLoader; 
 use Twig\Environment;


class Controller {
   
    public $params;
    public $layout = 'default';

    public function __construct($params)
    {
        $this->params = $params;
    } 

   public function render(){    
    require '..\app\views\layouts\\' . $this->layout . '.twig';
    $path = '..\app\views\\'.$this->params['controller'];
    $loader = new FilesystemLoader($path);
    $twig = new Environment($loader);
    return $twig;
   }
    
}