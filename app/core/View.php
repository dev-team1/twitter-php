<?php

namespace App\core;


class View
{

    public $path;
    public $layout = 'default';


    public function __construct($route)
    {

        $this->path = $route['controller'].'/'.$route['action'];
        
    }

    // public function render($title, $vars = [])
    // {
    //     extract($vars);  
    //     $path = '../app/views/pages/' . $this->path. '.php';  
    //     if (file_exists($path)) {
    //         ob_start();
    //         require $path;
    //         $content = ob_get_clean();
    //         require '../app/views/layouts/' . $this->layout . '.php';
    //     }else{
    //         echo 'View is not found: '. $this->path;
    //     }
    // }

    // public function redirect($url){
    //     header('location: '.$url);
    //     exit;
    // }

    public static function errorCode($code){
        http_response_code($code);
        $path = '../app/views/errors/' . $code . '.php';
        if(file_exists($path)){
            require $path;
        }
        exit;
    }

    
}
