<?php

class FollowSys
{
    private $db;

    public function __construct()
    {
        $this->db = Application::$db;
    }

    public function follow($follower_id, $following_id){}

    public function unfollow($follower_id, $following_id){}

    public function findFollow($follower_id, $following_id){}

    public function isFollow($follower_id, $following_id){}

    public function getTotalFollowing(){}

    public function getTotalFollower(){}

    public function getTotalFollowingByUserName($username){}

    public function getTotalFollowersByUserName($username){}

    public function getFollowingByUserName($username){}

    public function getFollow($id){}

    public function getFollowersByUserName($username){}
}






