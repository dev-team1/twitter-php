<?php

class Tweet
    {
        private $db;

        public function __construct()
        {
            $this->db = Application::$db;
        }

        public function getAllTweets(){}

        public function getAllTweetsByUserSession(){}

        public function getAllTweetsByUserName($username){}

        public function postTweet($body){}

        public function getTotalTweets(){}

        public function getTotalTweetsByUserName($username){}

        public function like($user_id, $tweet_id){}

        public function unlike($user_id, $tweet_id){}

        public function isLike($user_id, $tweet_id){}

        public function getTotalLikes($tweet_id){}

        public function deleteTweet($id){}
    }










