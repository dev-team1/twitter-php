<?php

class User
    {
        private $db;

        public function __construct()
        {
            $this->db = Application::$db;
        }

        public function signup($data){}

        public function getUserByUserName($username){}

        public function login($data){}

        public function searchForUser($user){}

        public function findEmailOrUsername($value){}

        public function getUserById(){}

        public function getAllUser($number = 0){}

        public function updateInfo($info){}

        public function updatePassword($password){}
    }


