let createBtn = document.getElementById('add-tweet');
let formTweet = document.querySelectorAll('.tweet-form');
let roots = document.getElementById('root');
let charCount = document.querySelectorAll('.character-counter');
let tweetForm = document.getElementById('tweet-form');
let time = new Date();



function tweet() {
  
  // if (formTweet[0].value === '') {
  //   alert('Write text!');
  // } else {
   
    const newTable = document.createElement('div');
    newTable.className =
      'flex-tweet row container col-lg-12 border-dar tweet mt-3 pb-2';
    roots.insertBefore(newTable, roots.firstChild);

    const tweetOf = document.createElement('div');
    tweetOf.className = 'h4 text-dark font-weight-normal pl-2 pt-3';
    tweetOf.innerHTML = document.querySelectorAll('.tweet-form')[0].value;
    newTable.appendChild(tweetOf);

    const avatar = document.createElement('div');
    avatar.className = 'fas fa-user-circle user-avatar  rounded-circle';
    newTable.insertBefore(avatar, newTable.firstChild);

    const timeTweet = document.createElement('div');
    timeTweet.className = 'h6 pl-2 pt-4';
    timeTweet.innerHTML = time.getHours() + ':' + time.getMinutes();
    newTable.appendChild(timeTweet);

    const btn = document.createElement('div');
    btn.className = 'btn__new-tweet d-flex';
    newTable.appendChild(btn);

    const like = document.createElement('button');
    like.className = 'fas fa-heart flip-icon pr-2 p icon btn';
    btn.appendChild(like);

    const del = document.createElement('button');
    del.className = 'fas fa-trash-alt pr-2 icon btn';
    btn.appendChild(del);

    formTweet[0].value = '';
    current.innerHTML='0';
    createBtn.disabled =true;
  //}
}

if (createBtn!==null){
  createBtn.disabled = true; 
}

//characterCount
$('textarea').keyup(function() {
  
  var characterCount = $(this).val().length,
      current = $('#current'),
      maximum = $('#maximum'),
      tweetForm = $('#tweet-form')

      if (characterCount>0){
         createBtn.disabled =false;
      }

  current.text(characterCount); 
  if (characterCount >= 200) {
    current.text('Your twit is too large!', characterCount);
    createBtn.disabled =true;
    maximum.css('color', '#8f0001');
    current.css('color', '#8f0001');
    tweetForm.css('border-color', '#8f0001')
  }else{
    maximum.css('color', 'black');
    current.css('color', 'black');
    tweetForm.css('border-color', 'black')
  }
});

// sidebar function
$(document).ready(function () {

  $('#sidebarCollapse').on('click', function () {
      $('#sidebar').toggleClass('active');
  });

  });

  if (createBtn!==null){
    createBtn.addEventListener('click', tweet);
  }



  let validInputs = 0; // to count how many inputs are valid
  let validation = document.getElementById('submit-button');
  let validationLogin = document.getElementById('login-button');
  let validationUpdate = document.getElementById('update-inform');
  let changePassword = document.getElementById('update-password');
  
  function invalidFunc(invalid, input) {
      // when the input is invalid
      invalid.style.display = 'inline-block';
      input.style.boxShadow = '0px 2px #f0695a';
  }
  
  function validFunc(invalid, input) {
      // when the input is valid
      invalid.style.display = 'none';
      input.style.border = 'none';
      input.style.boxShadow = '0px 2px #ced4da';
      validInputs++;
  }
  
      function validateBio() {
          let input = document.getElementById('bio');
          let invalid = document.getElementById('invalid-bio');
          if (
              /(.){8,1000}/.test(input.value) == false) {
              invalidFunc(invalid, input); // make it invalid when regex is failed
          } else {
              validFunc(invalid, input); // make it valid when regex is fine
          }
      }
  
  function validatePassword() {
      let input = document.getElementById('pass');
      let invalid = document.getElementById('invalid-password');
      if (
          /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&_.])[A-Za-z\d@$!%*?&_.]{8,}$/.test(
              input.value
          ) == false
      ) {
          invalidFunc(invalid, input); // make it invalid when regex is failed
      } else {
          validFunc(invalid, input); // make it valid when regex is fine
      }
  }   
  
  //------------------------CHANGE PASSWORD------------------------------------
  
      function validateCurrentPassword() {
          let input = document.getElementById('curr-pass');
          let invalid = document.getElementById('invalid-curr-pass');
          if (
              /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&_.])[A-Za-z\d@$!%*?&_.]{8,}$/.test(
                  input.value
              ) == false
          ) {
              invalidFunc(invalid, input); // make it invalid when regex is failed
          } else {
              validFunc(invalid, input); // make it valid when regex is fine
          }
      }
  //----------------------------------------------------------------------------
  
  
  
  function validatePasswordRepeat() {
      let pass = document.getElementById('pass');
      let input = document.getElementById('pass-repeat');
      let invalid = document.getElementById('invalid-pass-repeat');
      if (input.value != pass.value) {
          invalidFunc(invalid, input); // make it invalid when regex is failed
      } else {
          validFunc(invalid, input); // make it valid when regex is fine
      }
  }
  
  function validateEmail() {
      let input = document.getElementById('email');
      let str = input.value;
      let invalid = document.getElementById('invalid-email');
      if (
          /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/.test(str) ===
          false
      ) {
          invalidFunc(invalid, input); // make it invalid when regex is failed
      } else {
          validFunc(invalid, input); // make it valid when regex is fine
      }
  }
  
  function validateDate() {
      let input = document.getElementById('date');
      let str = input.value;
      let invalid = document.getElementById('invalid-date');
      if (/\d{4}-\d{1,2}-\d{1,2}/.test(str) === false) {
          invalidFunc(invalid, input); // make it invalid when regex is failed
      } else {
          validFunc(invalid, input); // make it valid when regex is fine
      }
  }
  
  function validateFirstName() {
      let input = document.getElementById('firstName');
      let invalid = document.getElementById('invalid-first-name');
      const reg = /[a-zA-Z]{3,30}/g;
      if (reg.test(input.value) != true) {
          invalidFunc(invalid, input); // make it invalid when regex is failed
      } else {
          validFunc(invalid, input); // make it valid when regex is fine
      }
  }
  
  function validateLastName() {
      let input = document.getElementById('lastName');
      let invalid = document.getElementById('invalid-last-name');
      const reg = /[a-zA-Z]{3,30}/g;
      if (reg.test(input.value) != true) {
          invalidFunc(invalid, input); // make it invalid when regex is failed
      } else {
          validFunc(invalid, input); // make it valid when regex is fine
      }
  }
  
  function validateUsername() {
      let input = document.getElementById('username');
      let invalid = document.getElementById('invalid-username');
      const reg = /[a-zA-Z0-9]{3,15}/g;
      if (reg.test(input.value) != true) {
          invalidFunc(invalid, input); // make it invalid when regex is failed
      } else {
          validFunc(invalid, input); // make it valid when regex is fine
      }
  }
  
  $("input").keyup(function() {
      var characterCount = $(this).val().length,
          current = $('#current'),
          maximum = $('#maximum'),
          tweetForm = $('#tweet-form')
          current.text(characterCount); 
          if (characterCount >= 1000) {
          current.text('Your bio is too large!', characterCount);
            maximum.css('color', '#8f0001');
            current.css('color', '#8f0001');
          }else{
              maximum.css('color', 'black');
              current.css('color', 'black');
              tweetForm.css('border-color', 'black')
            }
      });
  
  if(changePassword != null) {
      changePassword.onclick = function () {
          validInputs = 0;
          validatePassword();
          validatePasswordRepeat();
          if (validInputs == 2) {
              window.location.href = '../html/inform.php';
          } else {
              let formControls = document.querySelectorAll('.password-control'); // after button is clicked validate on every change of input
              for (const formControl of formControls) {
                  formControl.addEventListener('change', function (event) {
                      validatePassword();
                      validatePasswordRepeat();
                  });
              }
          }
      }
  }
  
  if(validationUpdate != null){
      validationUpdate.onclick = function () {
          //inform.html validation
          validInputs = 0;
          validateFirstName(); // checking all inputs
          validateEmail();
          validateLastName();
          validateUsername();
          validateDate();
          validateBio();
          if (validInputs == 6) {
              window.location.href = '../html/inform.php';
          } else {
              let formControls = document.querySelectorAll('.form-control'); // after button is clicked validate on every change of input
              for (const formControl of formControls) {
                  formControl.addEventListener('change', function (event) {
                      validateFirstName(); // checking all inputs
                      validateEmail();
                      validateLastName();
                      validateUsername();
                      validateDate();
                      validateBio();
                  });
              }
          }
      }
  }
  
  if(validationLogin != null) { // validation on login page
      validationLogin.onclick = function () {
          validInputs = 0;
          validateEmail();
          validatePassword();
          if (validInputs == 2) {
              // if all inputs are fine go to profile page
              window.location.href = '../html/profile.php';
          } else {
              let formControls = document.querySelectorAll('.form-control'); // after button is clicked validate on every change of input
              for (const formControl of formControls) {
                  formControl.addEventListener('change', function (event) {
                      validateEmail();
                      validatePassword();
                  });
              }
          }
      }
  }
  
  if(validation != null) {  // validation on register page
      validation.onclick = function () {
          //on submit button clicked
          validInputs = 0;
          validateFirstName(); // checking all inputs
          validateEmail();
          validateLastName();
          validateUsername();
          validateDate();
          validatePassword();
          validatePasswordRepeat();
          if (validInputs == 7) {
              // if all inputs are fine go to profile page
              window.location.href = '../html/profile.php';
          } else {
              let formControls = document.querySelectorAll('.form-control'); // after button is clicked validate on every change of input
              for (const formControl of formControls) {
                  formControl.addEventListener('change', function (event) {
                      validateFirstName();
                      validateEmail();
                      validateLastName();
                      validateUsername();
                      validateDate();
                      validatePassword();
                      validatePasswordRepeat();
                  });
              }
          }
      }
  }